const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

let build_js = './resources/assets/js/build.js';
let app_js = './public/js/app.js';

elixir((mix) => {
    mix.sass('app.scss')
        .copy('node_modules/patternfly/dist/fonts', 'public/fonts')
        .scripts([
            'bootstrap.js',
            'components.js',
            'app.js'
        ], build_js)
        .webpack(build_js, app_js);
});
