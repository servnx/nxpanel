<?php

use Illuminate\Console\Parser;

class ConsoleParserTest extends PHPUnit_Framework_TestCase
{
    public function testMakeModuleParsing()
    {
        $results = Parser::parse('make:module
        {slug : The slug of the module}
        {owner : The owner}
        {group : The group}
        {--quick : Skip the make:module wizard and use default values}');

        $this->assertEquals('make:module', $results[0]);
        $this->assertEquals('slug', $results[1][0]->getName());
        $this->assertEquals('owner', $results[1][1]->getName());
        $this->assertEquals('group', $results[1][2]->getName());
        $this->assertEquals('quick', $results[2][0]->getName());
        $this->assertFalse($results[2][0]->acceptValue());
    }
}