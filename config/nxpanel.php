<?php

return [
    /**
     * Monitor States
     */
    'monitor_states' => [
        'No State' => [
            'slug' => 'no_state',
            'description' => 'Has No State'
        ],
        'Unknown' => [
            'slug' => 'unknown',
            'description' => 'Is in an UNKNOWN state'
        ],
        'Ok' => [
            'slug' => 'ok',
            'description' => 'Is OK'
        ],
        'Info' => [
            'slug' => 'info',
            'description' => 'Has information'
        ],
        'Warning' => [
            'slug' => 'warning',
            'description' => 'Has Warnings'
        ],
        'Critical' => [
            'slug' => 'critical',
            'description' => 'Is Critical'
        ],
        'Error' => [
            'slug' => 'error',
            'description' => 'Has Errors'
        ]
    ],

    /**
     * Roles
     */
    'roles' => [
        'SuperAdmin' => [
            'slug' => 'superadmin',
            'description' => 'This is the main system user, can\'t be edited',
            'special' => 'all-access'
        ],

        'Admin' => [
            'slug' => 'admin',
            'description' => 'This User will have FULL access to the interface.',
            'special' => 'all-access'
        ],

        'Reseller' => [
            'slug' => 'reseller',
            'description' => 'User has elevated permissions in respects to a reseller.',
            'special' => null
        ],

        'Client' => [
            'slug' => 'client',
            'description' => 'User is a Client, Can manage own accounts ONLY.',
            'special' => null
        ],

        'Banned' => [
            'slug' => 'banned',
            'description' => 'This user has No Access to the interface.',
            'special' => 'no-access'
        ]
    ],

    /**
     * Permissions
     */
    'permissions' => [
        //
    ],
];