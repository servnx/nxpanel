<?php

return [

    /**
     * Dynamically generated from config/nxpanel.php@monitor_states
     * You should not edit this.
     *
     * @returns array of monitor states
     */
    'monitor_states' => function() {
        return array_keys(config('nxpanel.monitor_states'));
    }
];