<?php
//***  Ubuntu 16.04 default settings

return [

    // Distro
    'distname' => 'ubuntu1604',
    'min_php_version' => '5.3.3', // todo: should this be distro specific ? the paths reflect php7.0 ??

    // Server
    'server_name' => 'server1',
    'hostname' => 'server1.domain.tld',
    'ip' => '127.0.0.1',
    'domain' => 'domain.tld',
    'db_driver' => 'mysql',
    'is_slave' => false,

    // Main Paths
    'init_scripts' => '/etc/init.d',
    'runlevel' => '/etc',
    'shells' => '/etc/shells',
    'pam' => '/etc/pam.d',

    // Interface
    'interface' => [
        'interface_installed' => false,
        'app_key' => '',
        'nxpanel_install_dir' => '/usr/local/nxpanel',
        'nxpanel_config_dir' => '/usr/local/nxpanel/config',
    ],

    //* Services provided by this server
    'services' => [
        'mail' => false,
        'web' => false,
        'dns' => false,
        'file' => false,
        'db' => true, // must provide!
        'vserver' => false,
        'proxy' => false,
        'firewall' => false,
        'xmpp' => false
    ],

    //* DB
    'db' => [
        'installed' => false, // will be detected automatically during installation
        'host' => config('database.connections.db.host'),
        'port' => config('database.connections.db.port'),
        'database' => config('database.connections.db.database'),
        'username' => config('database.connections.db.username'),
        'password' => config('database.connections.db.password'),


        // interface admin
        'nxpanel_user' => 'nxpanel',
        'nxpanel_password' => '',

        // multi-server db settings, slaves require these to be set
        'master_host' => '',
        'master_port' => '3306',
        'master_database' => 'dbnxpanel',
        'master_admin_user' => 'root',
        'master_admin_password' => '',

        // master interface admin
        'master_nxpanel_user' => 'nxpanel',
        'master_nxpanel_password' => ''
    ],

    //* Apache
    'apache' => [
        'installed' => false, // will be detected automatically during installation
        'user' => 'www-data',
        'group' => 'www-data',
        'init_script' => 'apache2',
        'version' => '2.4',
        'vhost_conf_dir' => '/etc/apache2/sites-available',
        'vhost_conf_enabled_dir' => '/etc/apache2/sites-enabled',
        'vhost_port' => '8080',
        'php_ini_path_apache' => '/etc/php/7.0/apache2/php.ini',
        'php_ini_path_cgi' => '/etc/php/7.0/cgi/php.ini'
    ],

    //* Website base settings
    'web' => [
        'website_basedir' => '/var/www',
        'website_path' => '/var/www/clients/client[client_id]/web[website_id]',
        'website_symlinks' => '/var/www/[website_domain]/:/var/www/clients/client[client_id]/[website_domain]/',

        //* Apps base settings
        'apps_vhost_ip' => '_default_',
        'apps_vhost_port' => '8081',
        'apps_vhost_servername' => '',
        'apps_vhost_user' => 'ispapps',
        'apps_vhost_group' => 'ispapps'
    ],

    //* Fastcgi
    'fastcgi' => [
        'fastcgi_phpini_path' => '/etc/php/7.0/cgi/',
        'fastcgi_starter_path' => '/var/www/php-fcgi-scripts/[system_user]/',
        'fastcgi_bin' => '/usr/bin/php-cgi'
    ],

    //* Postfix
    'postfix' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/postfix',
        'init_script' => 'postfix',
        'user' => 'postfix',
        'group' => 'postfix',
        'vmail_userid' => '5000',
        'vmail_username' => 'vmail',
        'vmail_groupid' => '5000',
        'vmail_groupname' => 'vmail',
        'vmail_mailbox_base' => '/var/vmail'
    ],

    //* Mailman
    'mailman' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/mailman',
        'init_script' => 'mailman'
    ],

    //* mlmmj
    'mlmmj' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/mlmmj'
    ],

    //* Getmail
    'getmail' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/getmail',
        'program' => '/usr/bin/getmail'
    ],

    //* Courier
    'courier' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/courier',
        'courier-authdaemon' => 'courier-authdaemon',
        'courier-imap' => 'courier-imap',
        'courier-imap-ssl' => 'courier-imap-ssl',
        'courier-pop' => 'courier-pop',
        'courier-pop-ssl' => 'courier-pop-ssl'
    ],

    //* Dovecot
    'dovecot' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/dovecot',
        'init_script' => 'dovecot'
    ],

    //* SASL
    'saslauthd' => [
        'installed' => false, // will be detected automatically during installation
        'config' => '/etc/default/saslauthd',
        'init_script' => 'saslauthd',
    ],

    //* Amavisd
    'amavis' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/amavis',
        'init_script' => 'amavis'
    ],

    //* ClamAV
    'clamav' => [
        'installed' => false, // will be detected automatically during installation
        'init_script' => 'clamav-daemon'
    ],

    //* Pureftpd
    'pureftpd' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/pure-ftpd',
        'init_script' => 'pure-ftpd-mysql'
    ],

    //* MyDNS
    'mydns' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc',
        'init_script' => 'mydns'
    ],

    //* PowerDNS
    'powerdns' => [
        'installed' => false, // will be detected automatically during installation
        'database' => 'powerdns',
        'config_dir' => '/etc/powerdns/pdns.d',
        'init_script' => 'pdns'
    ],

    //* BIND DNS Server
    'bind' => [
        'installed' => false, // will be detected automatically during installation
        'bind_user' => 'root',
        'bind_group' => 'bind',
        'bind_zonefiles_dir' => '/etc/bind',
        'named_conf_path' => '/etc/bind/named.conf',
        'named_conf_local_path' => '/etc/bind/named.conf.local',
        'init_script' => 'bind9'
    ],

    //* Jailkit
    'jailkit' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/jailkit',
        'jk_init' => 'jk_init.ini',
        'jk_chrootsh' => 'jk_chrootsh.ini',
        'jailkit_chroot_app_programs' => '/usr/bin/groups /usr/bin/id /usr/bin/dircolors /usr/bin/lesspipe /usr/bin/basename /usr/bin/dirname /usr/bin/nano /usr/bin/pico /usr/bin/mysql /usr/bin/mysqldump /usr/bin/git /usr/bin/git-receive-pack /usr/bin/git-upload-pack /usr/bin/unzip /usr/bin/zip /bin/tar /bin/rm /usr/bin/patch',
        'jailkit_chroot_cron_programs' => '/usr/bin/php /usr/bin/perl /usr/share/perl /usr/share/php'
    ],

    //* Squid
    'squid' => [
        'installed' => false, // will be detected automatically during installation
        'config_dir' => '/etc/squid',
        'init_script' => 'squid'
    ],

    //* Nginx
    'nginx' => [
        'installed' => false, // will be detected automatically during installation
        'user' => 'www-data',
        'group' => 'www-data',
        'config_dir' => '/etc/nginx',
        'vhost_conf_dir' => '/etc/nginx/sites-available',
        'vhost_conf_enabled_dir' => '/etc/nginx/sites-enabled',
        'init_script' => 'nginx',
        'vhost_port' => '8080',
        'cgi_socket' => '/var/run/fcgiwrap.socket',
        /*
         * $conf['nginx']['php_fpm_init_script'] = '';
$conf['nginx']['php_fpm_ini_path'] = '';
$conf['nginx']['php_fpm_pool_dir'] = '';
         */
        'php_fpm_init_script' => 'php7.0-fpm',
        'php_fpm_ini_path' => '/etc/php/7.0/fpm/php.ini',
        'php_fpm_pool_dir' => '/etc/php/7.0/fpm/pool.d',
        'php_fpm_start_port' => 9010,
        'php_fpm_socket_dir' => '/var/lib/php7.0-fpm'
    ],

    //* OpenVZ
    'openvz' => [
        'installed' => false
    ],

    //*Bastille-Firwall
    'bastille' => [
        'installed' => false,
        'config_dir' => '/etc/Bastille'
    ],

    //* vlogger
    'vlogger' => [
        'config_dir' => '/etc'
    ],

    //* cron
    'cron' => [
        'init_script' => 'cron',
        'crontab_dir' => '/etc/cron.d',
        'wget' => '/usr/bin/wget'
    ],

    //* Metronome XMPP
    'xmpp' => [
        'installed' => false,
        'init_script' => 'metronome'
    ]
];