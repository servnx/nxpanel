<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

// Standard server side routes
Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index');

    Route::group(['prefix' => 'modules', 'middleware' => 'role:superadmin'], function() {
        Route::get('/', 'ModuleController@install');
        Route::get('/show', 'ModuleController@show');
        Route::get('/create', 'ModuleController@create');
        Route::get('/documentation', 'ModuleController@docs');
    });
});

// Api Routes (local api only)
Route::group(['middleware' => 'role:superadmin', 'namespace' => 'Api\\v1', 'prefix' => 'api/v1'], function() {

    Route::group(['prefix' => 'modules'], function() {
        Route::any('', 'ModulesApiController@api');
    });

});

// Authorization
Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login'])->middleware('web', 'guest');
    Route::post('login', ['uses' => 'LoginController@login'])->middleware('web', 'guest');
    Route::post('logout', ['uses' => 'LoginController@logout', 'as' => 'logout'])->middleware('web');

    Route::group(['prefix' => 'password'], function() {
        Route::post('email', ['uses' => 'ForgotPasswordController@sendResetLinkEmail'])->middleware('web','guest');
        Route::get('reset', ['uses' => 'ForgotPasswordController@showLinkRequestForm'])->middleware('web','guest');
        Route::post('reset', ['uses' => 'ResetPasswordController@reset'])->middleware('web','guest');
        Route::get('reset/{token}', ['uses' => 'ResetPasswordController@showResetForm'])->middleware('web','guest');
    });
});
