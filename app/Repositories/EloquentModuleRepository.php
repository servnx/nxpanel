<?php

namespace App\Repositories;

use App\Contracts\ModuleRepositoryInterface;
use Illuminate\Support\Facades\Artisan;
use Nwidart\Modules\Repository as BaseModuleRepository;

use App\Module;

class EloquentModuleRepository extends BaseModuleRepository implements ModuleRepositoryInterface
{
    public function scan()
    {
        $modules = parent::scan();

        foreach ($modules as $name => $obj) {
            $module = Module::whereName($name)->first();

            if ($module === null) {
                /*
                 * Create a new EloquentModule if it doesn't exist
                 */
                Module::create([
                    'name' => $obj->name,
                    'alias' => $obj->get('alias'),
                    'description' => $obj->get('description')
                ]);
            } else {
                /*
                 * If the module exists lets just make sure that
                 * its values are in sync with the module.json manifest
                 */
                $module->update([
                    'name' => $obj->name,
                    'alias' => $obj->get('alias'),
                    'description' => $obj->get('description'),
                    'active' => $obj->get('active'),
                    'protected' => $obj->get('protected'),
                    'menuitem' => $obj->get('menuitem')
                ]);
            }
        }

        return $modules;
    }

    public function fetchAll()
    {
        return Module::all();
    }

    public function create($name)
    {
        $exitCode = Artisan::call('module:make', [
            'name' => [$name]
        ]);

        if ($exitCode != 0) {
            return false;
        }

        return true;
    }

    public function remove($name)
    {
        $module = $this->whereName($name);
        $id =

        // delete from disk
        $this->delete($name);

        //delete from database
        $module->delete();

        return $module->id;
    }

    public function enable($name)
    {
        return $this->updateModule($name, 'active', true);
    }

    public function disable($name)
    {
        return $this->updateModule($name, 'active', false);
    }

    public function protect($name)
    {
        return $this->updateModule($name, 'protected', true);
    }

    public function unprotect($name)
    {
        return $this->updateModule($name, 'protected', false);
    }

    public function addToMenu($name)
    {
        return $this->updateModule($name, 'menuitem', true);
    }

    public function removeFromMenu($name)
    {
        return $this->updateModule($name, 'menuitem', false);
    }

    public function whereName($name)
    {
        return Module::whereName($name)->first();
    }

    public function getComposerAttributes($name)
    {
        return $this->findOrFail($name)
            ->json('composer.json')
            ->getAttributes();
    }

    public function authors($name)
    {
        return $this->getComposerAttributes($name)['authors'];
    }

    public function packageName($name)
    {
        return $this->getComposerAttributes($name)['name'];
    }

    public function packageDescription($name)
    {
        return $this->getComposerAttributes($name)['description'];
    }

    /**
     * @param $name
     * @param $key
     * @param $value
     * @return \Nwidart\Modules\Module
     */
    protected function updateModule($name, $key, $value)
    {
        $parent = $this->findOrFail($name);

        $this
            ->whereName($name)
            ->update([
                $key => $value
            ]);

        $parent
            ->json()
            ->set($key, ($value ? 1 : 0))
            ->save();

        return $parent;
    }
}