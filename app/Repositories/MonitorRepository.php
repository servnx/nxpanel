<?php

namespace App\Repositories;

use App\Contracts\MonitorInterface;
use App\Monitor;
use App\Traits\MonitorTools;

class MonitorRepository implements MonitorInterface {
    use MonitorTools;

    /**
     * @var Monitor
     */
    private $monitor;

    public function __construct(Monitor $monitor)
    {
        $this->monitor = $monitor;
    }

    public function create($params)
    {
        if (!is_array($params))
            throw new \Exception('argument must be an array');

        return $this->monitor->create($params);
    }

    public function insert($params)
    {
        return $this->create($params);
    }

    public function update($params)
    {
        if (!is_array($params))
            throw new \Exception('argument must be an array');

        return $this->monitor->update($params);
    }

    public function delete($id)
    {
        $monitor = $this->monitor->find($id);

        if ($monitor) {
            $monitor->delete();
        }
    }

    public function findByType($type)
    {
        return $this->monitor->where('type', $type)->get();
    }
}