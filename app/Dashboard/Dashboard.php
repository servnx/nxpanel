<?php

namespace App\Dashboard;

use App\Dashboard\Contracts\DashboardInterface;
use App\Salt\Contracts\GrainsInterface;
use App\Salt\Contracts\StatusInterface;

class Dashboard implements DashboardInterface
{
    /**
     * @var GrainsInterface
     */
    private $grains;

    /**
     * @var StatusInterface
     */
    private $status;

    public function __construct(GrainsInterface $grains, StatusInterface $status)
    {
        $this->grains = $grains;
        $this->status = $status;
    }

    public function getDistroDescription($target)
    {
        return $this->grains->getDistroDescription($target);
    }

    public function getOsFamily($target)
    {
        return $this->grains->getOsFamily($target);
    }

    public function getCpuModelName($target)
    {
        return $this->status->getCpuModelName($target);
    }

    public function getLoadAvg($target)
    {
        return $this->status->getLoadAvg($target);
    }

    public function getMaxLoadAvg($target)
    {
        return (1.0 * $this->status->getCpuCores($target));
    }

    public function getLoadPercentage($target)
    {
        return round(($this->getLoadAvg($target)['15-min'] / $this->getMaxLoadAvg($target)) * 100);
    }

    public function getAvailableLoadPercentage($target)
    {
        return round(100 - $this->getLoadPercentage($target));
    }

    public function getMemTotal($target)
    {
        return $this->status->getMemTotal($target);
    }

    public function getMemAvailable($target)
    {
        return $this->status->getMemAvailable($target);
    }

    public function getMemUsed($target)
    {
        return ($this->getMemTotal($target)['value'] - $this->getMemAvailable($target)['value']);
    }

    public function getUsedMemPercentage($target)
    {
        return round(($this->getMemUsed($target) / $this->getMemTotal($target)['value']) * 100);
    }

    public function getAvailableMemPercentage($target)
    {
        return round(100 - $this->getUsedMemPercentage($target));
    }
}