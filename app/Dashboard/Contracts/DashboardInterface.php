<?php

namespace App\Dashboard\Contracts;

interface DashboardInterface
{
    public function getDistroDescription($target);

    public function getOsFamily($target);

    public function getCpuModelName($target);

    public function getLoadAvg($target);

    public function getMaxLoadAvg($target);

    public function getMemTotal($target);

    public function getMemAvailable($target);

    public function getMemUsed($target);

    public function getLoadPercentage($target);

    public function getAvailableLoadPercentage($target);

    public function getUsedMemPercentage($target);

    public function getAvailableMemPercentage($target);
}