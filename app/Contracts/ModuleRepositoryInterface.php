<?php

namespace App\Contracts;

use Nwidart\Modules\Contracts\RepositoryInterface as BaseRepositoryInterface;

interface ModuleRepositoryInterface extends BaseRepositoryInterface
{
    public function fetchAll();

    public function getComposerAttributes($name);

    public function authors($name);

    public function packageName($name);

    public function packageDescription($name);

    public function create($name);

    public function remove($name);

    public function enable($name);

    public function disable($name);

    public function protect($name);

    public function unprotect($name);

    public function addToMenu($name);

    public function removeFromMenu($name);

    public function whereName($name);
}