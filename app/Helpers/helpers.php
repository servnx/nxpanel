<?php

use Illuminate\Support\Facades\Cache;

if (!function_exists('fqdn')) {
    function fqdn()
    {
        return trim(exec('sudo hostname -f'));
    }
}

if (!function_exists('hostname')) {
    function hostname()
    {
        return trim(exec('sudo hostname'));
    }
}

if (!function_exists('ip')) {
    function ip()
    {
        return trim(explode(' ', exec('sudo hostname -I'))[0]);
    }
}

if(!function_exists('ips')) {
    /**
     * @return array
     */
    function ips() {
        return explode(' ', exec('sudo hostname -I'));
    }
}

if (!function_exists('progressBarColor')) {
    function progressBarColor($percentage)
    {
        if ($percentage >= 60 && $percentage < 90) {
            return 'progress-bar-warning';
        }

        if ($percentage >= 90) {
            return 'progress-bar-danger';
        }

        return 'progress-bar-success';
    }
}

if (!function_exists('ProgressLabel')) {
    function progressLabel($percentage)
    {
        if ($percentage >= 60 && $percentage < 90) {
            return [
                'class' => 'label-warning',
                'text' => 'Warning'
            ];
        }

        if ($percentage >= 90) {
            return [
                'class' => 'label-danger',
                'text' => 'DANGER!'
            ];
        }

        return [
            'class' => 'label-success',
            'text' => 'OK'
        ];
    }
}

if (!function_exists('array_find')) {
    function array_find($searchKey, $array)
    {
        //create a recursive iterator to loop over the array recursively
        $iter = new RecursiveIteratorIterator(
            new RecursiveArrayIterator($array),
            RecursiveIteratorIterator::SELF_FIRST);

        //loop over the iterator
        foreach ($iter as $key => $value) {
            //if the key matches our search
            if ($key === $searchKey) {
                //add the current key
                $keys = array($key);
                //loop up the recursive chain
                for ($i = $iter->getDepth() - 1; $i >= 0; $i--) {
                    //add each parent key
                    array_unshift($keys, $iter->getSubIterator($i)->key());
                }
                //return our output array
                return array('path' => implode('.', $keys), 'value' => $value);
            }
        }

        //return false if not found
        return false;
    }
}