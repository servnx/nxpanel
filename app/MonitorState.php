<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonitorState extends Model
{
    protected $guarded = [];

    protected $table = "monitor_states";

    public function monitors() {
        return $this->hasMany(Monitor::class, 'state_id');
    }
}
