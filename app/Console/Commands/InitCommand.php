<?php

namespace App\Console\Commands;

use App;
use App\MonitorState;
use App\Server;
use App\User;
use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Console\Command;
use File;

class InitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nxpanel:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes NxPanel';

    /**
     * The configuration loaded from init.json
     *
     * @var $conf
     */
    private $conf;

    private $seperator = '/[:\.]/';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (File::exists(base_path('init.json'))) {
            $this->conf = json_decode(File::get(base_path('init.json')), true);
        } else {
            $this->error('init.json not found ...');
            exit(1);
        }

        $this->roles();

        $this->permissions();

        /**
         * Create the Administrator account and attach the SuperAdmin role!
         */
        $hostname = $this->get('hostname');
        $pass = $this->get('password');

        User::create([
            'name' => 'Administrator',
            'email' => "admin@$hostname",
            'password' => bcrypt($pass),
        ])->assignRole(1);

        $this->monitor();

        $this->server();

        // broadcast the event
        event('nxpanel.installed');

        exit(0);
    }

    private function server()
    {
        // Persist Server data
        Server::create([
            'name' => $this->get('server_name'),
            'hostname' => $this->get('hostname'),
            'ip' => $this->get('ip'),
            'mail_server' => false,
            'web_server' => true,
            'dns_server' => false,
            'file_server' => false,
            'db_server' => true,
            'vserver_server' => false,
            'proxy_server' => false,
            'firewall_server' => false,
            'xmpp_server' => false,
        ]);
    }

    private function roles()
    {
        $roles = config('nxpanel.roles');

        foreach ($roles as $role => $arr) {
            Role::create([
                'name' => $role,
                'slug' => $arr['slug'],
                'description' => $arr['description'],
                'special' => $arr['special']
            ]);
        }
    }

    private function permissions()
    {
        $permissions = config('nxpanel.permissions');

        foreach ($permissions as $perm => $arr) {
            Permission::create([
                'name' => $perm,
                'slug' => $arr['slug'],
                'description' => $arr['description'],
                'special' => $arr['special']
            ]);
        }
    }

    public function monitor()
    {
        $states = config('nxpanel.monitor_states');

        foreach ($states as $state => $arr) {
            MonitorState::create([
                'name' => $state,
                'slug' => $arr['slug'],
                'description' => $arr['description']
            ]);
        }
    }

    private function get($path, $default = null)
    {
        $array = $this->conf;

        if (!empty($path)) {
            $keys = $this->explode($path);
            foreach ($keys as $key) {
                if (isset($array[$key])) {
                    $array = $array[$key];
                } else {
                    return $default;
                }
            }
        }

        return $array;
    }

    private function explode($path)
    {
        return preg_split($this->seperator, $path);
    }
}
