<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerIp extends Model
{
    protected $guarded = [];

    public function server()
    {
        return $this->belongsTo(Server::class);
    }
}
