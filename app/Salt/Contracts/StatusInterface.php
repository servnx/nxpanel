<?php

namespace App\Salt\Contracts;

interface StatusInterface
{
    public function getCpuModelName($target = '*');

    public function getLoadAvg($target = '*');

    public function getCpuCores($target = '*');

    public function getMemTotal($target = '*');

    public function getMemAvailable($target = '*');
}