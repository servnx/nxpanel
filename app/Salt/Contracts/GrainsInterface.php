<?php

namespace App\Salt\Contracts;

interface GrainsInterface
{
    public function getDistroDescription($target = '*');

    public function getOsFamily($target = '*');
}