<?php

namespace App;

use Salt\Salt as BaseSalt;
use Salt\Utilities\SaltProcess;

class Salt extends BaseSalt
{
    public function cmd($cmd, $target = '*')
    {
        return (new SaltProcess())->execute("sudo salt '$target' " . $cmd);
    }
}