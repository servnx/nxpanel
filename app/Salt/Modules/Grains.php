<?php

namespace App\Salt\Modules;

use App\Salt\Contracts\GrainsInterface;
use Cache;
use Salt\Modules\Grains as SaltGrains;

class Grains extends SaltGrains implements GrainsInterface
{
    public function getDistroDescription($target = '*')
    {
        return Cache::remember($target . '_distrib_description', 5, function () use ($target) {
            return $this->item('lsb_distrib_description', $target);
        });
    }

    public function getOsFamily($target = '*')
    {
        return Cache::remember($target . '_os_family', 5, function () use ($target) {
            return $this->item('os_family', $target);
        });
    }
}