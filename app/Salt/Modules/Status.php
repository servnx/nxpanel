<?php

namespace App\Salt\Modules;

use App\Salt\Contracts\StatusInterface;
use Cache;
use Salt\Modules\Status as SaltStatus;

class Status extends SaltStatus implements StatusInterface
{
    public function getCpuModelName($target = '*')
    {
        return Cache::remember($target . '_cpu_name', 5, function () use ($target) {
            return $this->cpuinfo($target, 'model name');
        });
    }

    public function getLoadAvg($target = '*')
    {
        return Cache::remember($target . '_load_avg', 5, function () use ($target) {
            return $this->loadavg($target);
        });
    }

    public function getCpuCores($target = '*')
    {
        return Cache::remember($target . '_cpu_cores', 5, function () use ($target) {
            return $this->cpuinfo($target, 'cpu cores');
        });
    }

    public function getMemTotal($target = '*')
    {
        return Cache::remember($target . '_mem_total', 5, function () use ($target) {
            return $this->meminfo($target, 'MemTotal');
        });
    }

    public function getMemAvailable($target = '*')
    {
        return Cache::remember($target . '_mem_available', 5, function () use ($target) {
            return $this->meminfo($target, 'MemAvailable');
        });
    }
}