<?php

namespace App\Providers;


use App\Contracts\ModuleRepositoryInterface;
use App\Repositories\EloquentModuleRepository;
use Nwidart\Modules\LaravelModulesServiceProvider;


class ModulesServiceProvider extends LaravelModulesServiceProvider
{

    /**
     * Register the service provider.
     */
    protected function registerServices()
    {
        $this->app->singleton('modules', function ($app) {
            $path = $app['config']->get('modules.paths.modules');

            return new EloquentModuleRepository($app, $path);
        });

        $this->app->bind(ModuleRepositoryInterface::class, EloquentModuleRepository::class);
    }
}