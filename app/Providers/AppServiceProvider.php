<?php

namespace App\Providers;

use App\Console\Commands\InitCommand;
use File;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Contracts\MonitorInterface::class,
            \App\Repositories\MonitorRepository::class
        );

        $this->app->bind(
            \App\Dashboard\Contracts\DashboardInterface::class,
            \App\Dashboard\Dashboard::class
        );

        // commands
        $commands = [
            'command.nxpanel.init' => InitCommand::class
        ];

        foreach ($commands as $slug => $class) {
            $this->app->singleton($slug, function ($app) use ($slug, $class) {
                return $app[$class];
            });

            $this->commands($slug);
        }

        // helpers
        $path = realpath(__DIR__.'/../Helpers');
        $helpers = File::glob($path.'/*.php');

        foreach ($helpers as $helper) {
            require_once $helper;
        }
    }
}
