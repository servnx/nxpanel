<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SaltServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('salt', function ($app) {
            return new \App\Salt();
        });

        $tools = [
            \Salt\Modules\SaltUtil::class => 'salt.util',
            \Salt\Modules\Service::class => 'salt.service',
            \App\Salt\Modules\Grains::class => 'salt.grains',
            \Salt\Modules\Status::class => 'salt.status'
        ];

        foreach ($tools as $class => $id) {
            $this->app->singleton($id, function ($app) use ($class) {
                return new $class($app['salt']);
            });
        }

        $this->app->bind(
            \Salt\Contracts\SaltInterface::class,
            \App\Salt::class
        );

        $this->app->bind(
            \App\Salt\Contracts\GrainsInterface::class,
            \App\Salt\Modules\Grains::class
        );

        $this->app->bind(
            \App\Salt\Contracts\StatusInterface::class,
            \App\Salt\Modules\Status::class
        );
    }
}
