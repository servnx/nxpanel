<?php

namespace App\Http\Controllers;

use App\Contracts\ModuleRepositoryInterface;

class ModuleController extends Controller {

    private $module;

    public function __construct(ModuleRepositoryInterface $module)
    {
        $this->module = $module;
    }

    public function install()
    {

        return view('modules.install');
    }

    public function show()
    {
        return view('modules.show');
    }

    public function create()
    {
        return view('modules.create');
    }

    public function docs()
    {
        return view('modules.docs');
    }
}