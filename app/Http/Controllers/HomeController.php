<?php

namespace App\Http\Controllers;

use App\Dashboard\Contracts\DashboardInterface;
use App\Module;

class HomeController extends Controller
{


    /**
     * @var DashboardInterface
     */
    private $dashboard;

    public function __construct(DashboardInterface $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hostname = hostname();

        return view('dashboard.home', [
            'distrib_description' => $this->dashboard->getDistroDescription($hostname),
            'os_family' => $this->dashboard->getOsFamily($hostname),
            'cpu_name' => $this->dashboard->getCpuModelName($hostname),
            'load' => (float)$this->dashboard->getLoadAvg($hostname)['15-min'],
            'max_load' => (float)$this->dashboard->getMaxLoadAvg($hostname),
            'load_percentage' => $this->dashboard->getLoadPercentage($hostname),
            'available_load_percentage' => $this->dashboard->getAvailableLoadPercentage($hostname),
            'mem_total' => $this->dashboard->getMemTotal($hostname),
            'mem_used' => $this->dashboard->getMemUsed($hostname),
            'mem_percentage' => $this->dashboard->getUsedMemPercentage($hostname),
            'available_mem_percentage' => $this->dashboard->getAvailableMemPercentage($hostname)
        ]);
    }
}
