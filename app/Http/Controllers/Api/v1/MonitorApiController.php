<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Contracts\MonitorInterface;
use App\Traits\HandlesApiRequests;

class MonitorApiController extends Controller {
    use HandlesApiRequests;

    /**
     * The Monitor Instance
     *
     * @var MonitorInterface
     */
    private $monitor;

    public function __construct(MonitorInterface $monitor)
    {
        $this->monitor = $monitor;
    }

    public function create($params) {

    }
}