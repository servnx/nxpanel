<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Contracts\ModuleRepositoryInterface;
use App\Traits\HandlesApiRequests;
use Validator;

class ModulesApiController extends Controller
{
    use HandlesApiRequests;

    protected $get_methods = [
        'all'
    ];

    protected $post_methods = [
        'create',
        'getComposerAttributes'
    ];

    protected $patch_methods = [
        'enable',
        'disable',
        'protect',
        'unprotect',
        'addToMenu',
        'removeFromMenu'
    ];

    protected $put_methods = [
        //
    ];

    protected $delete_methods = [
        'remove'
    ];

    /**
     * The Module instance
     *
     * @var ModuleRepositoryInterface
     */
    private $module;

    /**
     * Injects Module dependency.
     * ModuleInterface resolves from service container to repository class.
     *
     * @param ModuleRepositoryInterface $module
     */
    public function __construct(ModuleRepositoryInterface $module)
    {
        $this->module = $module;
    }


    /**
     * Gets all modules
     *
     * @return Response
     */
    protected function all()
    {
        return response($this->module->fetchAll(), 200);
    }

    protected function getComposerAttributes($params = [])
    {
        $this->isValid($params);

        $data = $this->module->getComposerAttributes($params['name']);

        return response($data, 200);
    }

    /**
     * Creates a module
     *
     * @param array $params
     * @return Response
     */
    protected function create($params = [])
    {
        $this->isValid($params);

        if ($this->module->whereName($params['name']) !== null) {
            return response(['errors' => 'Module ' . $params['name'] . ' already exists!'], 422);
        }

        if (!$this->module->create($params['name'])) {
            return response(['errors' => 'An error occurred while creating module ' . $params['name']], 500);
        };

        return response($this->module->whereName($params['name']), 200);
    }

    public function remove($params = [])
    {
        $this->isValid($params);

        return response($this->module->remove($params['name']), 200);
    }

    /**
     * Enables a module
     *
     * @param array $params
     * @return Response
     */
    protected function enable($params = [])
    {
        $this->isValid($params);

        $this->module->enable($params['name']);

        return response($this->module->whereName($params['name']), 200);
    }

    /**
     * Disables a module
     *
     * @param array $params
     * @return Response
     */
    protected function disable($params = [])
    {
        $this->isValid($params);

        $this->module->disable($params['name']);

        return response($this->module->whereName($params['name']), 200);
    }

    /**
     * @param array $params
     * @return Response|bool
     */
    private function isValid($params)
    {
        $validator = Validator::make($params, [
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->failed()], 422);
        }

        return true;
    }

}