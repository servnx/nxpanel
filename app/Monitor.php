<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model
{
    private $guarded = [];

    public function state() {
        $this->hasOne(MonitorState::class, 'id', 'state_id');
    }
}
