<?php

namespace App\Traits;

trait ConsoleTools {

    protected function displayHeader($file = '', $level = 'info')
    {
        $stub = \File::get(resource_path('stubs/console/'.$file.'.stub'));

        return $this->$level($stub);
    }

    private function seperator()
    {
        $this->line("\n".str_repeat('=', 80)."\n");
    }

    private function head($title)
    {
        $this->seperator();
        $this->line($title);
        $this->seperator();
    }
}