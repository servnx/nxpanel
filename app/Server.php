<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $guarded = [];

    public function ips()
    {
        return $this->hasMany(ServerIp::class);
    }
}
