<?php

namespace App\Jobs;

use Artisan;
use Illuminate\Bus\Queueable;
use File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeModule implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * MakeModule constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->container = $params;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->container['slug'] === null)
            throw new \Exception('no slug parameter provided');

        $this->container['owner'] = (isset($this->container['owner']) ? $this->container['owner'] : null);
        $this->container['group'] = (isset($this->container['group']) ? $this->container['group'] : null);

        $this->container['name'] = studly_case($this->container['slug']);
        $this->container['version'] = '1.0';
        $this->container['description'] = 'This is the description for the '.$this->container['name'].' module.';
        $this->container['basename']    = studly_case($this->container['slug']);
        $this->container['namespace']   = config('modules.namespace').$this->container['basename'];

        $this->generate();
    }

    /**
     * Generate the module.
     */
    protected function generate()
    {
        $steps = [
            'generateModule',
            'optimizeModules',
            'generateController',
            'publishAssets',
        ];

        foreach ($steps as $function) {
            $this->$function();
        }

        event($this->container['slug'].'.module.made');
    }

    /**
     * Generate defined module folders.
     */
    protected function generateModule()
    {
        if (!File::isDirectory(module_path())) {
            File::makeDirectory(module_path());
        }

        $pathMap = config('modules.pathMap');
        $directory = module_path(null, $this->container['basename']);
        $source = resource_path('stubs/module');

        if(!File::isDirectory($directory)) {
            File::makeDirectory($directory);
        }

        $sourceFiles = File::allFiles($source, true);

        if (!empty($pathMap)) {
            $search = array_keys($pathMap);
            $replace = array_values($pathMap);
        }

        foreach ($sourceFiles as $file) {
            $contents = $this->replacePlaceholders($file->getContents());
            $subPath = $file->getRelativePathname();

            if (!empty($pathMap)) {
                $subPath = str_replace($search, $replace, $subPath);
            }

            $filePath = $directory.'/'.$subPath;
            $dir = dirname($filePath);

            if (!File::isDirectory($dir)) {
                File::makeDirectory($dir, 0755, true);
            }

            File::put($filePath, $contents);
        }

        $owner = $this->container['owner'];
        $group = $this->container['group'];

        if($owner !== null && $group !== null) {
            shell_exec('chown -R '.$owner.':'.$group.' '.$directory);
        }
    }

    /**
     * Generate a Module Controller.
     */
    protected function generateController()
    {
        Artisan::queue('make:module:controller',[
            'slug' => $this->container['slug'],
            'name' => studly_case($this->container['slug']).'Controller'
        ]);

        return true;
    }

    /**
     * Publish the modules Assets
     */
    protected function publishAssets() {
        Artisan::queue('vendor:publish',[
            '--tag' => 'modules',
            '--force' => true
        ]);

        return true;
    }

    /**
     * Reset module cache of enabled and disabled modules.
     */
    protected function optimizeModules()
    {
        Artisan::queue('module:optimize');

        return true;
    }

    protected function replacePlaceholders($contents)
    {
        $find = [
            'DummyBasename',
            'DummyNamespace',
            'DummyName',
            'DummySlug',
            'DummyVersion',
            'DummyDescription',
        ];

        $replace = [
            $this->container['basename'],
            $this->container['namespace'],
            $this->container['name'],
            $this->container['slug'],
            $this->container['version'],
            $this->container['description'],
        ];

        return str_replace($find, $replace, $contents);
    }
}
