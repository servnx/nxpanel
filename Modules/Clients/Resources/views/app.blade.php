<!-- Module Views MUST extend layouts.default.app -->
@extends('layouts.default.app')

<!--
    |--------------------------------------
    | View Partials
    |--------------------------------------
    |
    | The below partials are included by default.
    | Partials help keep your markup clean and separates responsibility.
    |
    | Note: You may edit the below partials respectfully.
    |
    | Keep in mind that we have provided templates to follow.
    |
    | The templates will help make sure that the UI stays consistent in its layout.
-->
@include('clients::partials.header')
@include('clients::partials.menu')