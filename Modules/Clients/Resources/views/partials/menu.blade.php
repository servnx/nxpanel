@section('menu')
    <ul class="nav nav-pills nav-stacked">
        <li>
            <a href="/clients">
                <i class="fa fa-edit"></i>
                Edit
            </a>
        </li>
        <li>
            <a href="/clients/create">
                <i class="fa fa-plus"></i>
                Add Client
            </a>
        </li>
        <li>
            <a href="/clients/email">
                <i class="fa fa-envelope"></i>
                Send Email
            </a>
        </li>
    </ul>
@endsection