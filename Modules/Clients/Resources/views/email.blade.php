<!-- Must extend app -->
@extends('clients::app')

<!-- Below is where we customize the Module View -->
@section('title')
    Send Email
@endsection

@section('content')
    <strong>Send email to clients</strong>
    <p>

    </p>
@endsection

@section('page-styles')
    <!--
        |--------------------------------------
        | View Styles
        |--------------------------------------
        |
        | Examples:
        |
        | <link href="{{asset('modules/clients/css/app.css')}}" rel="stylesheet">
        |
        | AND/OR
        |
        | <style>
        |    .client-header {
        |        background-color: #000011;
        |    }
        | </style>
    -->
@endsection

@section('scripts')
    <!--
        |--------------------------------------
        | View Scripts
        |--------------------------------------
        |
        | Examples:
        |
        | <script src="{{asset('modules/clients/js/create.js')}}"></script>
        |
        | AND/OR
        |
        | <script>
        |    $(document).ready(function() {
        |        console.log('Clients Module Loaded!');
        |    });
        | </script>
    -->
@endsection