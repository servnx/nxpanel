<!-- Must extend app -->
@extends('clients::app')

<!-- Below is where we customize the Module View -->
@section('title')
    Edit Client
@endsection

@section('content')
    <strong>This is the index of clients module</strong>
    <p>
        <!--
            |----------------------------------------------------
            | Example Vue component markup
            |----------------------------------------------------
            |
            | Below is using the Example.vue component located in
            | Assets/js/components.
            |
            | When you register your components its automatically prefixed
            | with the lowercase 'name' of your module.
            |
            | In this example, you can see its using '<clients-example>'.
            |
            | If I registered a 'MainMenu' component then it would become
            | '<clients-main-menu>' following the 'TitleCasedComponent' naming convention.
            |
            | Reference: https://vuejs.org/v2/guide/components.html#Component-Naming-Conventions
            |
            |
        -->
        <clients-example></clients-example>
    </p>
@endsection

@section('page-styles')
    <!--
        |--------------------------------------
        | View Styles
        |--------------------------------------
        |
        | Examples:
        |
        | <link href="{{asset('modules/clients/css/app.css')}}" rel="stylesheet">
        |
        | AND/OR
        |
        | <style>
        |    .client-header {
        |        background-color: #000011;
        |    }
        | </style>
    -->
@endsection

@section('scripts')
    <!--
        |--------------------------------------
        | View Scripts
        |--------------------------------------
        |
        | Examples:
        |
        | <script src="{{asset('modules/clients/js/create.js')}}"></script>
        |
        | AND/OR
        |
        | <script>
        |    $(document).ready(function() {
        |        console.log('Clients Module Loaded!');
        |    });
        | </script>
    -->
@endsection