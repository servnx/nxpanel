/*
 |--------------------------------------------------------------------------
 | Register Vue Components
 |--------------------------------------------------------------------------
 |
 | The Case-Sensitive filenames (excluding the .vue) of the components
 | you wish to register.
 |
 | Your components should be placed in Assets/js/components.
 |
 | We have been kind enough to provide you with an 'Example' component.
 |
 */
const components = [
    'Example'
];


/*
 |--------------------------------------------------------------------------
 | Exports a default object
 |--------------------------------------------------------------------------
 |
 | If you are here and not sure what to do, DO NOTHING! ;)
 |
 */
export default {
    components
}