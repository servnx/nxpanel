<?php

use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Database\Seeder;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->roles();
        $this->permissions();
    }

    private function roles()
    {
        $roles = config('nxpanel.roles');

        foreach($roles as $role => $arr) {
            Role::create([
                'name' => $role,
                'slug' => $arr['slug'],
                'description' => $arr['description'],
                'special' => $arr['special']
            ]);
        }
    }

    private function permissions()
    {
        $permissions = config('nxpanel.permissions');

        foreach($permissions as $perm => $arr) {
            Permission::create([
                'name' => $perm,
                'slug' => $arr['slug'],
                'description' => $arr['description'],
                'special' => $arr['special']
            ]);
        }
    }
}
