<?php

use Illuminate\Database\Seeder;
use App\MonitorState;

class MonitorStatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = config('nxpanel.monitor_states');

        foreach($states as $state => $arr) {
            MonitorState::create([
                'name' => $state,
                'slug' => $arr['slug'],
                'description' => $arr['description']
            ]);
        }
    }
}
