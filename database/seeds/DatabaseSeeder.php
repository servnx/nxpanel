<?php

use App\User;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Setup the default roles and permissions
         */
        $this->call(RolesPermissionsSeeder::class);

        /**
         * Create the Administraor account and attach the SuperAdmin role!
         */
        User::create([
            'name' => 'Administrator',
            'email' => 'admin@demo.com',
            'password' => bcrypt('demo'),
        ])->assignRole(1);

        /**
         * Setup the default Monitor States
         */
        $this->call(MonitorStatesSeeder::class);
    }
}
