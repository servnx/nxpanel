<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('hostname')->unique();
            $table->string('ip')->unique();
            $table->boolean('mail_server')->default(false);
            $table->boolean('web_server')->default(false);
            $table->boolean('dns_server')->default(false);
            $table->boolean('file_server')->default(false);
            $table->boolean('db_server')->default(false);
            $table->boolean('vserver_server')->default(false);
            $table->boolean('proxy_server')->default(false);
            $table->boolean('firewall_server')->default(false);
            $table->boolean('xmpp_server')->default(false);
            $table->boolean('is_active')->default(true);

            /*
             * If slave server
             */
            $table->boolean('is_minion')->default(false);
            $table->string('master_host')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
