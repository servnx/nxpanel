@extends('dashboard.view')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="page-header">
        <h3>Welcome {{Auth::user()->name}}</h3>
    </div>

    <!-- todo: alerts should be a dynamic component -->

    @role('superadmin')
    <div class="container-fluid container-cards-pf">
        <div class="row row-cards-pf">
            <div class="col-xs-6 col-sm-4 col-md-12">
                <div class="card-pf">

                    <div class="card-pf-heading">
                        <h2 class="card-pf-title">
                            {{$distrib_description}} ({{ $os_family }})
                        </h2>
                    </div>

                    <div class="card-pf-body">

                        <utilization-bar
                                desc="<strong>CPU:</strong> {{$cpu_name}}"
                                label="<strong>{{$load}} of {{$max_load}}</strong> 15-min Load Avg"
                                :valuemax="{{$max_load}}"
                                :percentage="{{$load_percentage}}"
                                :availpercentage="{{$available_load_percentage}}"
                                :thresholds="true"
                                :alerts="true"
                                :warn="50"
                                :danger="75"
                        ></utilization-bar>

                        <utilization-bar
                                desc="<strong>Memory</strong>"
                                label="<strong>{{$mem_used}} of {{$mem_total['value']}}</strong> {{$mem_total['unit']}}"
                                :valuemax="{{$mem_total['value']}}"
                                :percentage="{{$mem_percentage}}"
                                :availpercentage="{{$available_mem_percentage}}"
                                :thresholds="true"
                                :alerts="true"
                                :warn="65"
                                :danger="85"
                        ></utilization-bar>

                    </div>

                </div>

            </div>
        </div><!-- /row -->
    </div><!-- /container -->
    @endrole
@endsection

@section('page-styles')
    <style>

    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection