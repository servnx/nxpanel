<nav class="navbar navbar-default navbar-pf" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
            nx<span class="text-primary">Panel</span>
        </a>
        <span style="font-size: 14px; color: #dadada; padding-left: 10px">The Next Generation Open Source Server Management Interface!</span>
    </div>
    <div class="collapse navbar-collapse navbar-collapse-1">
        <ul class="nav navbar-nav navbar-utility">
            <li>
                <a href="/docs"><i class="fa fa-book"></i> Documentation</a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-user"></span>
                    {{Auth::user()->name}} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    @role('superadmin')
                    <li>
                        <a href="/tools">Tools</a>
                    </li>
                    @endrole

                    @role('superadmin')
                    <li>
                        <a href="/system">System</a>
                    </li>
                    <li>
                        <a href="/modules">Module Manager</a>
                    </li>
                    @endrole
                    <li class="divider"></li>
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Dynamic Modules Menu -->
        <main-menu :items="{{ \App\Module::where('active', true)->where('menuitem', true)->get() }}"></main-menu>
    </div>
</nav>
