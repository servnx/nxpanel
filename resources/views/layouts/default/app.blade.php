<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('head-meta')

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    @yield('page-styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('scripts-before')
</head>
<body>
    <div id="app">
        @if (!Auth::guest())
            @include('layouts.default.partials.top-nav')

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        @include('alerts.flash')
                        @include('alerts.errors')
                    </div>
                    <div class="col-md-10 col-md-offset-2">
                        <div class="page-header">
                            <h1>
                                @yield('page-header')
                                <small>@yield('page-header-subtext')</small>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        @yield('menu')
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-default">
                            <div class="panel-heading"><strong>@yield('title')</strong></div>

                            <div class="panel-body">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @yield('content')
        @endif
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>

    @yield('scripts')
</body>
</html>
