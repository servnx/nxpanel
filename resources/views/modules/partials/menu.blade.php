@section('menu')
    <ul class="nav nav-pills nav-stacked">
        <li>
            <a href="/modules">
                <i class="fa fa-download"></i>
                Install
            </a>
        </li>
        <li>
            <a href="/modules/show">
                <i class="fa fa-edit"></i>
                Edit
            </a>
        </li>
        <li>
            <a href="/modules/create">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </li>
        <li>
            <a href="/modules/documentation">
                <i class="fa fa-book"></i>
                Documentation
            </a>
        </li>
    </ul>
@endsection