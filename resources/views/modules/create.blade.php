@extends('modules.view')

@section('title')
    Generate a new module
@endsection

@section('content')
    <div class="alert alert-info">
        <p>
            This tool will create a new module directory with all the required boilerplate.
        </p>
    </div>
    <!-- todo: create vue component for form OR use jquery ajax request ??? -->
    <modules-create-form></modules-create-form>
@endsection

@section('page-styles')
    <style>

    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection