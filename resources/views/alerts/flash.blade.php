<?php
    $flash_msg_types = ['info','success','warning','danger'];
?>

@foreach($flash_msg_types as $type)
    @if(\Session::has("flash-{$type}"))
        <div class="alert alert-{{$type}}">
            {{ \Session::get("flash-{$type}") }}
        </div>
    @endif
@endforeach