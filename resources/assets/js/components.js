Vue.component('MainMenu', require('./components/MainMenu.vue'));
Vue.component('ModulesCreateForm', require('./components/modules/ModulesCreateForm.vue'));
Vue.component('ModulesListView', require('./components/modules/ModulesListView.vue'));
Vue.component('UtilizationBar', require('./components/UtilizationBar.vue'));

require('./module_components');