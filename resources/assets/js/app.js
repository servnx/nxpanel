import store from './vuex/store'

const app = new Vue({
    el: '#app',
    store,
    data: {
        modules: {}
    },
    methods: {
        setModules() {
            this.$http.get('/api/v1/modules', {
                'params': {
                    'action': 'all',
                    'params': {}
                }
            }).then((response) => {
                this.modules = response.body;
                this.$store.commit('SET_MODULES', this.modules);
            }, (response) => {
                this.alertShow('Error!', response.body.errors, 'danger');
            });
        }
    },

    mounted() {
        this.setModules();
    }
});