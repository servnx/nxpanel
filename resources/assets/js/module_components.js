// const fs = require('fs');
// const path = require('path');
//
// function getDirectories (srcpath) {
//     return fs.readdirSync(srcpath)
//         .filter(file => fs.statSync(path.join(srcpath, file)).isDirectory())
// }
//
// _.each(getDirectories('../../../Modules'), (module) => {
//
//     const obj = require(`../../../Modules/${module}/module.json`);
//
//     if (obj.enabled) {
//         const components = require(`../../../Modules/${module}/Assets/js/app`).components;
//         _.each(components, (component) => {
//             Vue.component(
//                 `${module.toLowerCase()}${component}`,
//                 require(`../../../Modules/${module}/Assets/js/components/${component}.vue`)
//             );
//         });
//     }
// });