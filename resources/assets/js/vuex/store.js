import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules/index'

// Make vue aware of Vuex
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        modules
    }
});