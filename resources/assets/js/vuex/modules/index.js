import * as types from './mutation-types'
import * as getters from './getters'
import * as actions from './actions'

export default {
    //namespaced: true,

    state: {
        items: []
    },

    getters,

    actions,

    mutations: {
        [types.SET_MODULES] (state, data) {
            state.items = data
        },

        [types.ADD_MODULE] (state, data) {
            state.items.push(data)
        },

        [types.UPDATE_MODULE] (state, data) {
            if (data) {
                for (let i in state.items) {
                    if (state.items[i].id === data.id) {
                        state.items.splice(i, 1, data);
                        break
                    }
                }
            }
        },

        [types.DELETE_MODULE] (state, id) {
            for (let i in state.items) {
                if (state.items[i].id === id) {
                    state.items.splice(i, 1);
                    break
                }

            }
        }
    }
};