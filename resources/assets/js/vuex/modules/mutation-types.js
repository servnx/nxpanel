export const SET_MODULES = 'SET_MODULES';
export const ADD_MODULE = 'ADD_MODULE';
export const UPDATE_MODULE = 'UPDATE_MODULE';
export const DELETE_MODULE = 'DELETE_MODULE';