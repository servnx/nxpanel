export default {
    setModules(context, items) {
        context.commit('SET_MODULES', items)
    },

    addModule(context, data) {
        context.commit('ADD_MODULE', data)
    },

    updateModule(context, id) {
        context.commit('UPDATE_MODULE', id)
    },

    deleteModule(context, id) {
        context.commit('DELETE_MODULE', id)
    }
}